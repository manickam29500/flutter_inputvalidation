import 'package:flutter/material.dart';
import 'package:flutter_input_validation/signUp.dart';
import 'package:localstorage/localstorage.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final LocalStorage storage = new LocalStorage('localstorage_app');
  late String email = "";
  late String password = "";

  void getData() {
    setState(() {
      email = storage.getItem('email');
      password = storage.getItem('password');
    });
    print('Data Fetched');
    toastMessage("Data Fetch", false);
  }

  void removeData() async {
    try {
      await storage.getItem('email');
      await storage.getItem('password');
      toastMessage("Data removed", false);
      print('Data removed');
      setState(() {
        email = "";
        password = "";
      });
    } catch (e) {
      print(e);
    }
  }

  void toastMessage(message, error) {
    Fluttertoast.showToast(
        msg: message, // message
        toastLength: Toast.LENGTH_SHORT, // length
        gravity: ToastGravity.BOTTOM, // location
        backgroundColor: error ? Colors.red : Colors.green,
        textColor: Colors.white,
        fontSize: 15);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Column(
              children: [
                Text('Email : ' + email),
                Text('Password : ' + password),
              ],
            ),
          ),
          Container(
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 50, top: 50),
                  child: ElevatedButton(
                    onPressed: () {
                      getData();
                    },
                    child: Text(
                      'Get Data',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 80, top: 50),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pop(context,
                          MaterialPageRoute(builder: (context) => SignUp()));
                    },
                    child: Text(
                      'Back',
                      style: TextStyle(fontSize: 25),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 130, top: 50),
            child: Row(
              children: [
                ElevatedButton(
                    onPressed: () {
                      removeData();
                    },
                    child: Text('Remove Data'))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
