import 'package:flutter/material.dart';
import 'package:flutter_input_validation/home.dart';
import 'package:localstorage/localstorage.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  late String emailError = "";
  late String passwordError = "";
  late String confirmPasswordError = "";

  late TextEditingController _emailController = TextEditingController();
  late TextEditingController _passwordController = TextEditingController();
  late TextEditingController _confirmPasswordController =
      TextEditingController();
  final LocalStorage storage = new LocalStorage('localstorage_app');

  void InputValidation(email, password, confirmPassword) {
    if (email == "") {
      setState(() {
        emailError = 'Field is Empty';
      });
    } else {
      if (RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(email)) {
        setState(() {
          emailError = "";
        });
      } else {
        setState(() {
          emailError = "Invalid Email Id";
        });
      }
    }

    if (password == '') {
      setState(() {
        passwordError = 'Field is Empty';
      });
    } else {
      if (RegExp(
              r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
          .hasMatch(password)) {
        setState(() {
          passwordError = "";
        });
      } else {
        setState(() {
          passwordError = "";
        });
      }
    }

    if (confirmPassword == '') {
      setState(() {
        confirmPasswordError = "Field is Empty";
      });
    } else {
      if (password == confirmPassword) {
        setState(() {
          confirmPasswordError = "";
        });
      } else {
        setState(() {
          confirmPasswordError = "Mismatch of Password";
        });
      }
    }

    if (emailError.length == 0 &&
        passwordError.length == 0 &&
        confirmPasswordError.length == 0) {
      saveToStorage(email, password);
    } else {
      toastMessage('Field is Empty', true);
    }
  }

  void saveToStorage(email, password) async {
    try {
      await storage.setItem('email', email);
      await storage.setItem('password', password);
      print('Data Saved');
      toastMessage("Data Saved in Local Storage", false);
    } catch (e) {
      print(e);
    }
  }

  void toastMessage(message, error) {
    Fluttertoast.showToast(
        msg: message, // message
        toastLength: Toast.LENGTH_SHORT, // length
        gravity: ToastGravity.BOTTOM, // location
        backgroundColor: error ? Colors.red : Colors.green,
        textColor: Colors.white,
        fontSize: 15);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        margin: EdgeInsets.only(top: 100),
        padding: EdgeInsets.all(8),
        child: Column(
          children: [
            TextField(
              controller: _emailController,
              decoration: new InputDecoration(
                  label: Text(
                    'Email',
                    style: TextStyle(fontSize: 25),
                  ),
                  errorText: emailError,
                  errorStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              style: TextStyle(fontSize: 20),
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              child: TextField(
                controller: _passwordController,
                decoration: new InputDecoration(
                    label: Text(
                      'Password',
                      style: TextStyle(fontSize: 25),
                    ),
                    errorText: passwordError,
                    errorStyle:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              child: TextField(
                controller: _confirmPasswordController,
                decoration: new InputDecoration(
                  errorText: confirmPasswordError,
                  errorStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  label: Text(
                    'Confirm Password',
                    style: TextStyle(fontSize: 25),
                  ),
                ),
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              child: Row(
                children: [
                 Container(
                   margin: EdgeInsets.only(left: 50),
                   child:  ElevatedButton(
                     onPressed: () {
                       InputValidation(
                           _emailController.text,
                           _passwordController.text,
                           _confirmPasswordController.text);
                     },
                     child: Text(
                       'Submit',
                       style: TextStyle(fontSize: 25),
                     ),
                   ),
                 ),
                  Container(
                    margin: EdgeInsets.only(left:80),
                    child: TextButton(
                        onPressed: () {
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => HomePage()));
                        },
                        child: Text(
                          'Next',
                          style: TextStyle(fontSize: 25),
                        )),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
